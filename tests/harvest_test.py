


import os, sys, json, unittest
from time import time

from mock import patch as obj_patch

sys.path.insert(0, sys.path[0]+"/..")

from harvest import (
    HarvestRestClient, HarvestError, HTTPContentType, 
    HTTPHeader, OauthKey, BasicKey,
    HTTPContentType
)

TEST_FIXTURE_DIR = os.path.dirname(__file__) + '/fixtures/'

def patch(json_file):
    json_ = json.load(open("%s/%s" % (TEST_FIXTURE_DIR, json_file)))
    return obj_patch.object(HarvestRestClient, "_request", return_value=json_)

class TestHarvest(unittest.TestCase):

    HARVEST_URI = "https://domain.harvestapp.com"
    TEST_ACCESS = 'access'
    TEST_REFRESH = 'refresh'

    def setUp(self):
        basic_kwargs = {
            BasicKey.EMAIL: "test@email.com",
            BasicKey.PASSWORD: "test_password"
        }
        oauth2_kwargs = {
            OauthKey.CLIENT_ID: 'Unittest'
        }

        self.basic_client = HarvestRestClient(self.HARVEST_URI, **basic_kwargs)
        self.oauth_client = HarvestRestClient(self.HARVEST_URI, **oauth2_kwargs)

        TEST_ACCESS = self.TEST_ACCESS
        TEST_REFRESH = self.TEST_REFRESH
        class MockRsp:
            status_code = 200
            def json(self):
                return {OauthKey.ACCESS_TOKEN: TEST_ACCESS, OauthKey.REFRESH_TOKEN: TEST_REFRESH}
        self.mocked_rsp = MockRsp()

    def test_invalid_uri(self):
        with self.assertRaises(HarvestError):
            HarvestRestClient("invalid_uri")

    def test_set_uri(self):
        self.assertEqual(self.basic_client.uri, self.HARVEST_URI)
        self.assertEqual(self.oauth_client.uri, self.HARVEST_URI)

    def test_set_oauth2(self):
        self.assertFalse(self.basic_client.oauth2, 'oauth2 property should be False')
        self.assertTrue(self.oauth_client.oauth2, 'oauth2 property should be True')

    def test_oauth2_properties(self):
        self.assertIsNotNone(self.oauth_client.authorize_url, 'authorize_url not set in oauth2 client')
        self.assertNotEqual(self.oauth_client.authorize_data, {}, 'authorize_data not set in oauth2 client')
        self.assertEqual(self.basic_client.authorize_data, {}, 'authorize_data is set in basic client')
        self.assertIsNone(self.basic_client.authorize_url, 'authorize_url is set in basic client')

    def test_basic_properties(self):
        self.assertIsNotNone(self.basic_client.email, 'email not set on basic client')
        self.assertIsNotNone(self.basic_client.password, 'password not set on basic client')
        self.assertIsNone(self.oauth_client.email, 'email set on oauth client')
        self.assertIsNone(self.oauth_client.password, 'password set on oauth client')

    def test_set_content_type(self):
        self.assertEqual(self.basic_client.headers.get(HTTPHeader.ACCEPT), 'application/json', "Accept header not set correctly.")
        new_client = HarvestRestClient(self.HARVEST_URI, content_type=HTTPContentType.XML, client_id='Test')
        self.assertEqual(new_client.headers.get(HTTPHeader.ACCEPT), HTTPContentType.XML, "Accept header not set correctly.")

    def test_set_headers(self):
        self.assertIn(HTTPHeader.AUTH, self.basic_client.headers.keys(), 'Auth header not set in basic client.')
        self.assertNotIn(HTTPHeader.AUTH, self.oauth_client.headers.keys(), 'Auth header set in oauth client.')

    def test_set_encoded_type(self):
        self.basic_client.set_form_encoded_type()
        self.assertEqual(self.basic_client.headers.get(HTTPHeader.CONTENT_TYPE), HTTPContentType.FORM_ENCODED)

    def test_reset_content_type(self):
        self.test_set_encoded_type()
        self.basic_client.reset_content_type()
        self.assertEqual(self.basic_client.headers.get(HTTPHeader.CONTENT_TYPE), HTTPContentType.JSON)

    def test_get_oauth_tokens(self):
        with obj_patch('requests.request', return_value=self.mocked_rsp) as get_tokens_mock:
            with self.assertRaises(HarvestError):
                self.basic_client.get_tokens('code', 'secret')
            access, refresh = self.oauth_client.get_tokens('code', 'secret')
            self.assertEqual(access, self.TEST_ACCESS)
            self.assertEqual(refresh, self.TEST_REFRESH)
            self.assertTrue(get_tokens_mock.called)

    def test_refresh_oauth_tokens(self):
        with obj_patch('requests.request', return_value=self.mocked_rsp) as refresh_tokens_mock:
            with self.assertRaises(HarvestError):
                self.basic_client.refresh_tokens('code', 'secret')
            access, refresh = self.oauth_client.refresh_tokens('code', 'secret')
            self.assertEqual(access, self.TEST_ACCESS)
            self.assertEqual(refresh, self.TEST_REFRESH)
            self.assertTrue(refresh_tokens_mock.called)

    def test_response_is_successful(self):
        "No difference between the clients on this method."
        self.assertTrue(self.basic_client.response_is_successful(200))
        self.assertFalse(self.basic_client.response_is_successful(300))

    def test_list_projects(self):
        with obj_patch.object(HarvestRestClient, '_request', return_value=json.load(open("%s/%s" % (TEST_FIXTURE_DIR, 'projects.json')))) as project_mock:
            self.assertTrue(isinstance(project_mock.return_value, list), '%s is not a list' % project_mock.return_value)

    def test_get_today(self):
        with patch('today.json') as today_mock:
            today = self.basic_client.today()
            self.assertTrue(today.get("for_day"))
            today_mock.assert_called_once_with('GET', '/daily', None, None)

    def test_add(self):
        add_data = {
            "notes": "%s" % "Some Notes",
            "hours": "1.5",
            "project_id": 1,
            "task_id": 1234
        }
        with patch('add_time.json') as add_mock:
            r = self.basic_client.add(add_data)
            add_mock.assert_called_once_with('POST', '/daily/add', add_data, params=None)
            self.assertTrue(r)

        with patch('add_time.json') as oauth2_mock:
            r = self.oauth_client.add(add_data, params={'access_token': self.TEST_ACCESS})
            oauth2_mock.assert_called_once_with('POST', '/daily/add', add_data, params={'access_token':self.TEST_ACCESS})
            self.assertTrue(r)

if __name__ == '__main__':
    unittest.main()
